
Commande pour hébergement sur <http://www.heroku.com>
=====================================================

Il faut avoir les sources sous git.

* Creation d'une application sous heroku

    	heroku create --stack cedar --buildpack https://github.com/heroku/heroku-buildpack-python.git leomedia

Pour installer l'application

	git push heroku master
	
* Installation base de développement ( - de 10000 lignes , gratuite)

		heroku addons:add heroku-postgresql:dev

* Nettoyage de la base

		heroku pg:reset HEROKU_POSTGRESQL_GREEN_URL --confirm leomedia

* Préchargement de la base

		heroku run python src/manage.py loaddata src/LEOGoals/fixtures/bd.0.3.0.json

* Mise en jour de la base (marche moyen car traitement long)

		time ./src/tv_program/tv_grab_fr_iphone.pl --days 7 | curl --data-binary @- http://leomedia.herokuapp.com/tvprogram/rest/pushprogram/
