#include "Arduino.h"
#include "IRLampCmd.h"
#include "IRLampCode.h"


// This procedure sends a 38KHz pulse to the IRledPin 
// for a certain # of microseconds. We'll use this whenever we need to send codes
static void pulseIR(int IRledPin, long microsecs) {
  // we'll count down from the number of microseconds we are told to wait
 
  cli();  // this turns off any background interrupts
 
  while (microsecs > 0) {
    // 38 kHz is about 13 microseconds high and 13 microseconds low
   digitalWrite(IRledPin, HIGH);  // this takes about 3 microseconds to happen
   delayMicroseconds(10);         // hang out for 10 microseconds, you can also change this to 9 if its not working
   digitalWrite(IRledPin, LOW);   // this also takes about 3 microseconds
   delayMicroseconds(10);         // hang out for 10 microseconds, you can also change this to 9 if its not working
 
   // so 26 microseconds altogether
   microsecs -= 26;
  }
 
  sei();  // this turns them back on
}
 
static void SendIRCode(int IRledPin, const int irPattern[], const int patternSize) {
  // This is the code for my particular Nikon, for others use the tutorial
  // to 'grab' the proper code from the remote
  
  int bufferPattern[patternSize];
  int*pbufferPattern= bufferPattern;
  Serial.print("size :"); Serial.println(patternSize);
  memcpy_P(bufferPattern, irPattern,patternSize*sizeof(int));
  do {
    //Serial.print("ON : ");
    //Serial.print(*pbufferPattern++*10);
    pulseIR(IRledPin,*pbufferPattern++*10l); // On in ten'th of microsecond
    delayMicroseconds(*pbufferPattern*10l); // Off in ten'th microsecond
    //Serial.print(" OFF : ");
    //Serial.println(*pbufferPattern*10);
  } while (*pbufferPattern++); // Ending when Off period is 0
}


#define SendIRPattern(ledPin, pattern) SendIRCode(ledPin, pattern, pattern##_size)

void IRLampCmd::setStatus(boolean on, boolean multiaction) {
  if (on)
    SendIRPattern(ledPin, IRLampOn);
  else
    SendIRPattern(ledPin, IRLampOff);
  if (multiaction) {
    Serial.println("pause for other action");
    delayMicroseconds(10000);
  }
}


void IRLampCmd::setColor(Colors color) {
  switch(color) {
    case RED:
      SendIRPattern(ledPin, IRLampRed);
      break;
    case GREEN:
      SendIRPattern(ledPin, IRLampGreen);
      break;
    case BLUE:
      SendIRPattern(ledPin, IRLampBlue);
      break;
    case WHITE:
      SendIRPattern(ledPin, IRLampWhite);
      break;
  }
  
}

void IRLampCmd::upIntensity() {
  SendIRPattern(ledPin, IRLampUp);
}

void IRLampCmd::downIntensity() {
  SendIRPattern(ledPin, IRLampDown);
}
