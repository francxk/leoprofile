#ifndef __IRLAMPCMD__
#define __IRLAMPCMD__

#include "LampColors.h" 

//===========================================================
//
// Commande de la lampe par infrarouge
// 
//===========================================================
class IRLampCmd {
  private:
    int ledPin;
    
  public:
    IRLampCmd(int ledPin): ledPin(ledPin) {};
    void setStatus(boolean status, boolean multiaction=false);
    void setColor(Colors color);
    void downIntensity();
    void upIntensity();
};

#endif
