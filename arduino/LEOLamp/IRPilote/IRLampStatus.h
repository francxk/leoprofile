#ifndef __IRLAMPSTATUS__
#define __IRLAMPSTATUS__

#include "IRLampCmd.h" 
#include "LampColors.h" 

//==========================================================
// Etat de la lampe
//   * color : sa coulour principale
//   * status : son etat on / off
//   * intensity : sa puissance
//==========================================================
class LampStatus {
  private:
    boolean status;
    Colors color;
    int intensity;
    IRLampCmd *lampCmd;
  
  public:
  
    IRLampCmd *setLampCmd(IRLampCmd* newLampCmd);
    
    boolean setStatus(boolean newStatus,boolean multiaction=false);
    boolean getStatus(void) { return status; };

    Colors setColor(Colors newColor) ;
    Colors getColor(void) { return color; };

    void upIntensity() ;
    void downIntensity() ;

    int setIntensity(int newIntensity) ;
    int getIntensity(void) { return intensity; };

};


#endif // __IRLAMPSTATUS__
