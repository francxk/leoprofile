#include "Arduino.h"
#include "IRLampStatus.h"


IRLampCmd* LampStatus::setLampCmd(IRLampCmd* newLampCmd) {
  IRLampCmd* prev = lampCmd; lampCmd= newLampCmd;
  return prev;
}

boolean LampStatus::setStatus(boolean newStatus, boolean multiaction) {
  boolean prev = status; status= newStatus;
  if (lampCmd != NULL) {
    lampCmd->setStatus(status, multiaction);
  }
  return prev;
}


Colors LampStatus::setColor(Colors newColor) {
  Colors prev = color; color= newColor;
  if (lampCmd != NULL) {
    if (! status) {// lamp Off
      setStatus(true, true);
    }
    lampCmd->setColor(color);
  }
  return prev;
}

int LampStatus::setIntensity(int newIntensity) {
  int prev = intensity;
  intensity= newIntensity > MAX_INTENSITY ? MAX_INTENSITY :
      newIntensity < MIN_INTENSITY ? MIN_INTENSITY : newIntensity;
  
  return prev;
}

void LampStatus::upIntensity() {
  if (intensity < MAX_INTENSITY) {
    intensity++;
    lampCmd->upIntensity();
  }
}

void LampStatus::downIntensity() {
  if (intensity > MIN_INTENSITY) {
    intensity--;
    lampCmd->downIntensity();
  }
}

