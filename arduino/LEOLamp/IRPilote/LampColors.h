#ifndef __LAMPCOLORS__
#define __LAMPCOLORS__

enum Colors {
  UKN=0, RED, GREEN, BLUE, WHITE
};

const int MIN_INTENSITY=0;
const int MAX_INTENSITY=3;

#endif
