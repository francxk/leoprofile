// This sketch will send out a Nikon D50 trigger signal (probably works with most Nikons)
// See the full tutorial at http://www.ladyada.net/learn/sensors/ir.html
// this code is public domain, please enjoy!

#include <CmdMessenger.h>

#include "description.h"

#include "IRLampStatus.h"
#include "IRLampCmd.h"

int IRledPin =  12;    // LED connected to digital pin 12 resistor 39 ohms
 

LampStatus IRLamp; // Status of the IRLamp
IRLampCmd lampCmd(IRledPin); // lamp controller


// Mustnt conflict / collide with our message payload data. Fine if we use base64 library ^^ above
const char field_separator = ',';
const char command_separator = ';';

// Attach a new CmdMessenger object to the default Serial port
CmdMessenger cmdMessenger = CmdMessenger(Serial, field_separator, command_separator);

// Commands we send from the Arduino to be received on the PC
enum
{
  kCOMM_ERROR    = 000, // Lets Arduino report serial port comm error back to the PC (only works for some comm errors)
  kACK           = 001, // Arduino acknowledges cmd was received
  kARDUINO_READY = 002, // After opening the comm port, send this cmd 02 from PC to check arduino is ready
  kERR           = 003, // Arduino reports badly formatted cmd, or cmd not recognised

  // Now we can define many more 'send' commands, coming from the arduino -> the PC, eg
  // kICE_CREAM_READY,
  // kICE_CREAM_PRICE,
  // For the above commands, we just call cmdMessenger.sendCmd() anywhere we want in our Arduino program.

  kSEND_CMDS_END, // Mustnt delete this line
};


// Commands we send from the PC and want to recieve on the Arduino.
// We must define a callback function in our Arduino program for each entry in the list below vv.
// They start at the address kSEND_CMDS_END defined ^^ above as 004
messengerCallbackFunction messengerCallbacks[] = 
{
  lamp_on,   //004
  lamp_off,  //005
  lamp_red,  //006
  lamp_green,//007
  lamp_blue, //008
  lamp_white,//009
  lamp_status, //010
  lamp_up, //011
  lamp_down,//012
  NULL
};


// ------------------ C A L L B A C K  M E T H O D S -------------------------

void lamp_on() {
  cmdMessenger.sendCmd(kACK, "Lamp On received");
  IRLamp.setStatus(true);
}
void lamp_off() {
  cmdMessenger.sendCmd(kACK, "Lamp Off received");
  IRLamp.setStatus(false);
}

void lamp_status() {
  cmdMessenger.sendCmd(kACK, "Lamp status received");
  char rep[100];
  sprintf(rep,"%d,%d,%d",IRLamp.getStatus(), IRLamp.getColor(),
      IRLamp.getIntensity());
  cmdMessenger.sendCmd(10 /*lamp status*/, rep);
}

void lamp_up() {
  cmdMessenger.sendCmd(kACK, "Lamp Up received");
  IRLamp.upIntensity();
}

void lamp_down() {
  cmdMessenger.sendCmd(kACK, "Lamp Down received");
  IRLamp.downIntensity();
}


void lamp_red() {
  cmdMessenger.sendCmd(kACK, "Lamp Red received");
  IRLamp.setColor(RED);
}
void lamp_green() {
  cmdMessenger.sendCmd(kACK, "Lamp Green received");
  IRLamp.setColor(GREEN);
}

void lamp_blue() {
  cmdMessenger.sendCmd(kACK, "Lamp Blue received");
  IRLamp.setColor(BLUE);
}
void lamp_white() {
  cmdMessenger.sendCmd(kACK, "Lamp White received");
  IRLamp.setColor(WHITE);
}

// ------------------ D E F A U L T  C A L L B A C K S -----------------------

void arduino_ready()
{
  // In response to ping. We just send a throw-away Acknowledgement to say "im alive"
  cmdMessenger.sendCmd(kACK,
                      _VERSION_MAJOR "," _VERSION_MINOR ","
                      _VERSION_DESC ","__TIMESTAMP__);
}

void unknownCmd()
{
  // Default response for unknown commands and corrupt messages
  cmdMessenger.sendCmd(kERR,"Unknown command");
}

// ------------------ E N D  C A L L B A C K  M E T H O D S ------------------

// ------------------ S E T U P ----------------------------------------------

void attach_callbacks(messengerCallbackFunction* callbacks)
{
  int i = 0;
  int offset = kSEND_CMDS_END;
  while(callbacks[i]) {
    cmdMessenger.attach(offset+i, callbacks[i]);
    i++;
  }
}




// The setup() method runs once, when the sketch starts 
void setup()   {                
  // initialize the IR digital pin as an output:
  pinMode(IRledPin, OUTPUT);      

  IRLamp.setLampCmd(&lampCmd);
 
  Serial.begin(9600);
      // cmdMessenger.discard_LF_CR(); // Useful if your terminal appends CR/LF, and you wish to remove them
  cmdMessenger.print_LF_CR();   // Make output more readable whilst debugging in Arduino Serial Monitor
  
  // Attach default / generic callback methods
  cmdMessenger.attach(kARDUINO_READY, arduino_ready);
  cmdMessenger.attach(unknownCmd);

  // Attach my application's user-defined callback methods
  attach_callbacks(messengerCallbacks);

  arduino_ready();
}
 
void loop()                     
{
      // Process incoming serial data, if any
  cmdMessenger.feedinSerialData();
}
 
  
