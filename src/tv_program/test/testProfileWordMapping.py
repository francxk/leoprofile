# -*- coding: utf-8 -*-

import logging
'''
Created on 11 nov. 2012

@author: franck
'''

from tv_program.scoring.matching import countWord, countWordForVignette, \
    countWordForProfile
from LEOGoals.models.Vignette import Vignette
from django.test import TestCase
from LEOGoals.models.OrderedVignetteSet import OrderedVignetteSet
import string

# Get an instance of a logger
logger = logging.getLogger("unittest")


class ProfileWordMappingTest(TestCase):

    fixtures = ['profileViolenceEducatif.json']

    def setUp(self):
        pass


    def tearDown(self):
        pass

    def testCountWordOnProfileEmptyContent(self):
        logger.debug("testCountWordOnProfileEmptyContent")
        profile = OrderedVignetteSet.objects.get(title="Violence")
        expected = [("Violence", dict()), ('Action', dict()),
                   ('BD', dict())]
        res = countWordForProfile(profile, "")
        self.assertEqual(expected, [(profile.title, score) for profile, score in res])

    def testCountWordOnProfile(self):
        logger.debug("testCountWordOnProfile")
        profile = OrderedVignetteSet.objects.get(title="Violence")
        expected = [("Violence", {'violence': 1}), ('Action', {'violence': 1}),
                   ('BD', {'bd': 3, 'dessin': 1})]
        res = countWordForProfile(profile,
                                  "bd  \n Bd, bof bof bd dessin violence educatif".lower().translate(string.maketrans("", ""), string.punctuation))
        self.assertEqual(expected, [(vignette.title, score) for vignette, score in res])
        
        
        


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
