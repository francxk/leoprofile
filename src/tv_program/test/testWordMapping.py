# -*- coding: utf-8 -*-

import logging
'''
Created on 11 nov. 2012

@author: franck
'''

from tv_program.scoring.matching import countWord, countWordForVignette
from LEOGoals.models.Vignette import Vignette
from django.test import TestCase

# Get an instance of a logger
logger = logging.getLogger("unittest")


class TestWordMapping(TestCase):

    fixtures = ['2vignettes.json']

    def setUp(self):
        pass


    def tearDown(self):
        pass


    def testEmptyCountWord(self):
        logger.debug("testEmptyCountWord")
        expected= dict()
        res = countWord([], "t t  t t motA mot2 mot2")
        self.assertEqual(expected, res)

    def testEmptyContentCountWord(self):
        logger.debug("testEmptyCountWord")
        expected= dict()
        res = countWord(["a", 'b'], "")
        self.assertEqual(expected, res)


    def testSimpleCountWord(self):
        logger.debug("testSimpleCountWord")
        expected= {"mot2":2}
        res = countWord(['mot1', 'mot2'], "motA mot2, ;mot2")
        self.assertEqual(expected, res)

        expected= {"mot2":2, "mot1":1}
        res = countWord(['mot1', 'mot2'], "motA mot2\n mot2 Mot1")
        self.assertEqual(expected, res)

    def testComplexPatternCountWord(self):
        logger.debug("testComplexCountWord")
        expected= {"mota mot2":1}
        res = countWord(['mota mot2'], "motA mot2, ;mot2")
        self.assertEqual(expected, res)

        expected= {"mot2":2, "mot1":1}
        res = countWord(['mot1', 'mot2'], "motA mot2\n mot2 Mot1")
        self.assertEqual(expected, res)

    def testCountWordOnVignettes(self):
        logger.debug("testCountWordOnVignettes")
        vignette=Vignette.objects.get(title="BD")
        expected= {"bd":3}
        res = countWordForVignette(vignette, "bd bd bof bof bd")
        self.assertEqual(expected, res)

        expected= {"bd":1, "dessin":2, "animation":1}
        res = countWordForVignette(vignette, "bd, et Animation rrr dessin ppp dessin")
        self.assertEqual(expected, res)

        vignette=Vignette.objects.get(title="Action")
        expected= {"bruce":1, "bruce willis":1}
        res = countWordForVignette(vignette, "bruce willis bd dessin bof violence")
        self.assertEqual(expected, res)
        
        
        


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()