# -*- coding: utf-8 -*-
'''
Created on 11 oct. 2012

@author: franck
'''
import logging
from rest_framework.views import APIView
from rest_framework.response import Response
from LEOGoals.models.OrderedVignetteSet import OrderedVignetteSet
from tv_program.scoring.matching import countWordForProfile
import string
from rest_framework.parsers import BaseParser
import re

logger = logging.getLogger('tvprogram.view')

class PlainTextParser(BaseParser):
    """
    Plain text parser.
    """

    media_type = 'text/plain'

    def parse(self, stream, media_type=None, parser_context=None):
        """
        Simply return a string representing the body of the request.
        """
        return stream.read()


class Scoring(APIView):
    """
    ###Evalutate score against scoreOnProfile
    In the url set the **scoreOnProfile number** at the end of the URL, ex <http://machine:8000/tvprogram/rest/scoring/1>
    and for ***heroku*** <http://leomedia.herokuapp.com/tvprogram/rest/scoring/1>
    
    Set the text to analyse below. The response has the form:
    
        [
            [
                "vignette name1", 
                {
                    "word1": 4, # nb of time keword1 is seen
                }
            ], 
            [
                "vignette name2", 
                {}
            ]
        ]           
    """
    parser_classes = (PlainTextParser,)
    
    def post(self, request, profile_id, format=None):
        logger.debug("Evalute score for scoreOnProfile %s" % profile_id)
        content = request.DATA
        scoreOnProfile = OrderedVignetteSet.objects.get(id=profile_id)

        res = countWordForProfile(scoreOnProfile, content)
        return Response([(vignette.title, score) for vignette, score in res])
