# -*- coding: utf-8 -*-
'''
Created on 11 oct. 2012

@author: franck
'''
import datetime
import logging
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.parsers import XMLParser
from rest_framework import status
from lxml import objectify
from dateutil.parser import parse
import re
from tv_program.models.TvChannel import TvChannel
from tv_program.models.TvProgram import TvProgram

logger = logging.getLogger('tvprogram.view')

class PushProgram(APIView):
    """
    ###Updating Tv program.
    Only xmltv style message allowed.
    
    Use the tv_grab_fr_iphone.pl script to get tv program then send to this web service

        ./tv_grab_fr_iphone.pl --days 7 > tmp.xmltv
        wget http://localhost:8000/tvprogram/rest/pushprogram/ --post-file tmp.xml
    ou
    
        ./tv_grab_fr_iphone.pl --days 7 | curl --data-binary @- http://localhost:8000/tvprogram/rest/pushprogram/
            
    ### Pointeurs et source d'origine
       * [Starting point by the author](http://www.moreau37.fr/index.php?option=com_content&view=article&id=63%3Aprogrammes-telerama-sur-mythtv&catid=38%3Amythtv&Itemid=70&limitstart=1)
       * <http://wiki.xmltv.org/index.php/XmltvCapabilities>
       
    """
    parser_classes = (XMLParser,)
    
    actor_regexp=re.compile("([^(]+)*\(?.*\)? *") # to keep only actor name, not character
    
    def post(self, request, format=None):
        logger.debug("program tv update requested")
        allprogram= objectify.fromstring(request.body)
        for program in allprogram.programme:
            #logger.debug([att for att in dir(program)])
            idchan = program.get("channel")
            start = parse(program.get("start"))
            stop = parse(program.get("stop"))
            idprg = program.get("showview") if program.get("showview") else program.get("start")+" " + idchan
            title = program.title.text
            subtitle = getattr(program, "sub-title").text if hasattr(program, "sub-title") else None
            icon = getattr(program, "icon").get("src") if hasattr(program, "icon") else None
            desc = program.desc.text
            ratings=dict([(rating.get("system"), rating.text) for rating in program.rating])
            categories=[category.text.strip() for category in program.category]
            persons = []
            if hasattr(program, "credits"):
                persons.extend([person.text for person in
                           program.credits.presenter]) if hasattr(program.credits, "presenter") else None
                persons.extend([person.text for person in
                           program.credits.guest]) if hasattr(program.credits, "guest") else None
                persons.extend([person.text for person in
                           program.credits.director]) if hasattr(program.credits, "director") else None
                persons.extend([self.__class__.actor_regexp.match(person.text).group(1).strip()
                                 for person in
                           program.credits.actor]) if hasattr(program.credits, "actor") else None
            
#            if logger.isEnabledFor(logging.DEBUG):
#                logger.debug("%s[%s,%s],%s/%s %s %s" % (idchan,start, stop, title,subtitle,ratings, categories))
#                logger.debug("  %s" % persons)
            # Save to database
            tvchannel, _ =TvChannel.objects.get_or_create(idchannel=idchan)
            tvprogram, newprog = TvProgram.objects.get_or_create(idprogram=idprg,
                                defaults={'start': start, 'end' : stop,
                                          'title':title, 'subtitle':subtitle,
                                          'icon':icon, 'desc':desc,
                                          'tvchannel':tvchannel})
            if not newprog:
                # Add actors as tag
                if persons:
                    tvprogram.tags.add(*[name.lower() for name in persons])
                # Add Categories as tag
                if categories:
                    tvprogram.tags.add(*[catname.lower() for catname in categories])
                # Add ratings as tag
                if ratings:
                    tvprogram.tags.add(*[sys.lower()+'-'+notation.lower() for sys,notation in ratings.iteritems()])

        return Response(status=status.HTTP_200_OK)
