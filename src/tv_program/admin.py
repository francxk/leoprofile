# -*- coding: utf-8 -*-

from django.utils.translation import ugettext_lazy as _
from django.contrib import admin
from taggit.models import Tag
from tv_program.models import TvChannel
from tv_program.models.TvProgram import TvProgram




class TvChannelAdmin(admin.ModelAdmin):
    list_display = ('idchannel', 'name', 'comments')
    search_fields = ['idchannel', 'name']

    fieldsets = (
        ('', {
            'fields': ('idchannel', 'name', ),
        }),
        (_(u'Details'), {
            'classes': ('grp-collapse grp-closed',),
            'fields' : ('comments',),
        }),
                 )

class TvProgramAdmin(admin.ModelAdmin):
    def thumb(self,instance):
        if not instance.icon:
            return ""
        else:
            return '<img src="' + instance.icon + '" alt="Vignette" height="60" width="60">'
    thumb.short_description = _(u'Thumbnail')
    thumb.allow_tags = True
    
    list_display = ('thumb', 'start', 'title', 'subtitle', 'tvchannel')
    search_fields = ['title', 'subtitle']
    list_editable=('subtitle', 'tvchannel',)
    list_display_links = ('thumb', 'start', 'title')

    fieldsets = (
        ('', {
            'fields': ('idprogram', 'tvchannel', 'icon', 'title', 'subtitle', 'start', 'end' ),
        }),
        (_(u'Details'), {
            'classes': ('grp-collapse grp-open',),
            'fields' : ('tags', 'desc',),
        }),
                 )


admin.site.register(TvChannel,TvChannelAdmin)
admin.site.register(TvProgram,TvProgramAdmin)
