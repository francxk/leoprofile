from django.conf.urls.defaults import *
from django.conf.urls import patterns, include, url
import logging
from rest_framework.urlpatterns import format_suffix_patterns

from tv_program import views

# Get an instance of a logger
logger = logging.getLogger('settings')

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'django_home_user_mgnt.views.home', name='home'),
    # url(r'^django_home_user_mgnt/', include('django_home_user_mgnt.foo.urls')),
    url(r'rest/pushprogram/$', views.PushProgram.as_view()),
    url(r'rest/scoring/(?P<profile_id>[0-9]+)$', views.Scoring.as_view()),
    
)

urlpatterns = format_suffix_patterns(urlpatterns)