#!/bin/bash
iphone_exec=~/Applications/tv_grab_fr_iphone.pl

lockf="/tmp/`basename $0`.pid"

# Grab a random value between 0-655.
value=`expr $RANDOM / 50`

if [ -z "$*" ]; then
  echo "syntax error"
  exit
fi

if [ "x$1" != "x-d" ]; then
  # Sleep for that time.
  echo "waiting $value s"
  sleep $value
else
 shift
fi

if [ -f $lockf ]; then
  echo $0 locked
  killall -9 `cat $lockf`
  rm $lockf
  exit
fi

echo "$$" > $lockf

echo "processing $*"

$iphone_exec --offset $* > /tmp/tvguide.xmltv
/usr/bin/mythfilldatabase --file 1 /tmp/tvguide.xmltv
/usr/bin/mythfilldatabase --file 2 /tmp/tvguide.xmltv

rm $lockf

