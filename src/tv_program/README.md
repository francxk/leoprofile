Programme TV
============

Gestion du programme TV.


Récupération des programme TV.
------------------------------

### Pointeurs et source d'origine
   * [Point de départ / auteur](http://www.moreau37.fr/index.php?option=com_content&view=article&id=63%3Aprogrammes-telerama-sur-mythtv&catid=38%3Amythtv&Itemid=70&limitstart=1)
   * <http://wiki.xmltv.org/index.php/XmltvCapabilities>


### Installation et dépendances logicielles
Le script de récupération des programmes TV nécessite:

 * Perl
 * Module Perl Date::Time, XML::Simple, DBI, Date/Manip

Commandes d'installation:

    :::bash
    sudo cpan
    install DateTime
    install XML::Simple
    install DBI
    install Date::Manip

Installer la configuration de tv grabber à la racine : voir exemple tv_grab_fr_iphone.conf

    :::bash
    cp tv_grab_fr_iphone.conf ~/.tv_grab_fr_iphone
    
### Récupération des programmes
    :::bash
    ./tv_grab_fr_iphone.pl --offset 7 > tmp.xmltv

