# -*- coding: utf-8 -*-
'''
Created on 11 nov. 2012

@author: franck
'''
from collections import Counter
import logging
import re
import string

logger=logging.getLogger('tvprogram')
regex_punctuation = re.compile('[%s]' % re.escape(string.punctuation))

def countWord(word_filter, text_content):
    '''
    Count words in text_content if present in word_filter.
    text_content is cleaning for punctuation symbol and lowered
    
    :param word_filter:
    :param text_content:
    :return :dict of word count
    '''
    text_content=' '.join(regex_punctuation.sub(' ', text_content.lower()).split())

    res={}
    for key in word_filter:
        count=text_content.count(key)
        if count!=0:
            res[key]=count 
    return res

def countWordForVignette(vignette, text_content):
    '''
    Count words in text_content if present in tags of vignette
    
    :param vignette:
    :param text_content:
    '''
    return countWord([tag.name for tag in vignette.tags.all()], text_content)

def countWordForProfile(orderedVignetteSet, text_content):
    
    res=[]
    for vignette in orderedVignetteSet.vignettes.all():
        score = countWordForVignette(vignette, text_content)
        res.append((vignette, score))
    return res
