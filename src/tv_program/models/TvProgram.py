# -*- coding: utf-8 -*-
'''
Created on 10 nov. 2012

@author: franck
'''
from django.db import models
from django.utils.translation import ugettext_lazy as _
import logging
from taggit_autosuggest.managers import TaggableManager
from tv_program.models.TvChannel import TvChannel

# Get an instance of a logger
logger = logging.getLogger('model')

class TvProgram(models.Model):
    '''
    TV program description
    '''

    idprogram=models.CharField(_(u"Program Identifier"), max_length=50, null=True)
    title=models.CharField(_(u"Title"), max_length=100)
    subtitle=models.CharField(_(u"Sub Tile"), max_length=100, blank=True, null=True)
    icon = models.URLField(_("icon url"),
                              null=True, blank=True)
    start=models.DateTimeField(_(u"Starting time"), blank=True, null=True, db_index=True)
    end=models.DateTimeField(_(u"Ending time"), blank=True, null=True)

    desc=models.TextField(_(u"Description"), max_length=300, blank=True, null=True)
 
    tvchannel = models.ForeignKey(TvChannel, verbose_name =
                                  _(u"TV Channel"),blank=True, null=True, on_delete=models.CASCADE)
    
    tags = TaggableManager(verbose_name=_(u"Persons, Categories..."),
                           help_text=_(u"Keywords/tags associated to Tv Program, it can be actors, or program categories..."),
                           blank=True)
    
    def __init__(self, *args, **kwargs):
        super(TvProgram, self).__init__(*args, **kwargs) 
        
    def __unicode__(self):
        return "" + "" + self.title
    
    # Cela semble obligatoire pour la generation de la base
    class Meta:
        app_label='tv_program'
        verbose_name = "TV Program Description"
        