# -*- coding: utf-8 -*-
'''
Created on 10 nov. 2012

@author: franck
'''
from django.db import models
from django.utils.translation import ugettext_lazy as _
import logging

# Get an instance of a logger
logger = logging.getLogger('model')

class TvChannel(models.Model):
    '''
    Descrition of the TV Channel
    '''

    name=models.CharField(_(u"Channel Name"), max_length=50,  blank=True, null=True)
    idchannel=models.CharField(_(u"Id of the channel"), max_length=50, unique=True, db_index=True)

    comments=models.TextField(_(u"Comments"), max_length=150, blank=True, null=True)
 
    def __init__(self, *args, **kwargs):
        super(TvChannel, self).__init__(*args, **kwargs) 
        
    def __unicode__(self):
        return self.name if self.name else self.idchannel
    
    # Cela semble obligatoire pour la generation de la base
    class Meta:
        app_label='tv_program'
        verbose_name = "TV Channel Description"
        