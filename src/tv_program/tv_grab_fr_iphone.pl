#!/usr/bin/perl -w
#
#  Retrieve EPG from Telerama, France
#
#  This file is subject to the terms and conditions of the GNU General Public License
#       Channel list available at: http://91.121.66.148/~verytv/procedures/ListeChaines.php
#  Compliant with DTD: http://membled.com/work/apps/xmltv/cvs_working/xmltv.dtd
#
# C.Moreau, (c) 2009, 2010, 2011, 2012
#
# Usage: see code
#
# Configuration file:
#  selected_channels (list): list of channels to retrieve
#  mythtv_dir (string): path of mythtv home directory
#  xmltvid_prefix (string): string to add at the begin to XMLTV id
#  xmltvid_postfix (string): string to add at the end to XMLTV id
#  desc_episode (0/1): add episode number to description
#  desc_crit (0/1): add critics to description
#  dst (0/1): check for Daylight saving
#
# Change log:
#  1.0: creation
#  1.1: Fixed programs going after midnight
#  1.2: Added dolby, fixed actors list, fixed star-rating. Added read from file (debug)
#  1.3: Fixed episode numbering
#  1.4: Changes few things to add flexibility into code
#  1.5: Treat specific tags in descriptions
#  1.6: Add others tags in descriptions
#  1.7: Cleanup Actors/Guests
#  1.8: Fixed Localtime changes
#  1.9: Add dump raw file (-d option)
#  2.0: add filter on name, use of config file for channels, Filter "<>", changed options parsing
#  2.1: Merged changes of E. Anne (update channels, list-channels, capabilities, ...). Many thanks to him for his work. Enhanced the config file. Added option to put episode number in description. Added option to have critics.
#  2.2: minor change. Changer release to separate from beta release of 2.1
#  2.3: fixed error messages
#  2.4: Changed command options to grabbers standard (incompatibles were moved to config file). Added multiples offsets. Complete code rewrite.
#       E. Anne modification/fixes:
#          - nouveaux noms pour tv5, direct star, et France O (tnt)
#          - ajustement de heures quand on change d'heure
#          - erreur de frappe dans la correction des heures
#  2.5: Changes for episode numbering (FabriceMG via Mythtv-fr)
#  2.6: Errors displayes to STDERR
#       Added option to use channels like C23.telerama.fr instead of 23.telerama.fr (xmltvid_prefix, xmltvid_postfix)
#       Added icon handling for programs
#       Added 4/3 format detection
#  2.7: Fixed problem with "16/9"->"16:9" tag change, "Tous publics" new tag.
#  2.8: Fixed <I>,</I>,<P>,</P> tags in critics,
#       "Sous-titrage"->"Sous-titr�" tag change, 
#       "In�dit"->"Premi�re diffusion" tag change,
#       Added "surround" handling.
#  2.8.1: Fixed episode index number
#  2.8.2: Esthetics changes
#  2.8.3: Remove <T> tags everywhere
#  2.8.4: Cleanup inside Critics
#  2.8.5: Fix of wrong dayligth saving
#  2.8.6: again for daylight saving
#  2.8.7: various warnings removed

use strict;
use warnings;
use LWP 5.64;
use POSIX qw(strftime);
use Encode;
use DateTime;
use Getopt::Long qw(:config auto_version);
use XML::Simple;
use DBI;
use Date::Manip qw(UnixDate);

require HTTP::Cookies;

#
# Constants
#
$main::VERSION = "2.8.7";

my $cnf_site_addr = "guidetv-iphone.telerama.fr";
my $cnf_site_prefix = "http://$cnf_site_addr/verytv/procedures/";
my $cnf_site_img = "http://$cnf_site_addr/verytv/procedures/images/";
my $cnf_useragt = 'Telerama/1.0 CFNetwork/445.6 Darwin/10.0.0d3';
my $cnf_progfile = "tv_grab_fr_iphone";
my $cnf_progname = "$cnf_progfile.pl";

# channel icons
# just look for "icones de chaines de television" on google, wikipedia is
# very good at it. Here are some, there are more...
my %cnf_icon_urls = (
                     1 => "http://upload.wikimedia.org/wikipedia/fr/thumb/8/85/TF1_logo_privatisation.svg/120px-TF1_logo_privatisation.svg.png",
                     2 => "http://upload.wikimedia.org/wikipedia/fr/thumb/9/97/France2.svg/71px-France2.svg.png",
                     3 => "http://upload.wikimedia.org/wikipedia/fr/thumb/d/d7/France3.svg/70px-France3.svg.png",
                     4 => "http://upload.wikimedia.org/wikipedia/fr/thumb/c/cc/Logo_canal.jpg/120px-Logo_canal.jpg",
                     5 => "http://upload.wikimedia.org/wikipedia/fr/thumb/a/a2/France5.svg/71px-France5.svg.png",
                     6 => "http://upload.wikimedia.org/wikipedia/fr/thumb/2/26/Logo-M6.svg/120px-Logo-M6.svg.png",
                     7 => "http://upload.wikimedia.org/wikipedia/fr/thumb/7/7e/ARTE_logo_1989.png/120px-ARTE_logo_1989.png",
                     8 => "http://upload.wikimedia.org/wikipedia/fr/thumb/c/c9/Direct8_new_2009.png/200px-Direct8_new_2009.png",
                     9 => "http://upload.wikimedia.org/wikipedia/fr/thumb/0/0f/W9_2010_logo.png/120px-W9_2010_logo.png",
                     10 => "http://upload.wikimedia.org/wikipedia/fr/4/4b/Logo_de_TMC.gif",
                     11 => "http://upload.wikimedia.org/wikipedia/fr/thumb/6/61/NT1.jpg/120px-NT1.jpg",
                     12 => "http://upload.wikimedia.org/wikipedia/fr/thumb/e/ea/NRJ12.png/200px-NRJ12.png",
                     13 => "http://upload.wikimedia.org/wikipedia/fr/7/74/Logo_france4_2008.png",
                     14 => "http://upload.wikimedia.org/wikipedia/en/thumb/a/a8/LCP-Public_Senat.png/200px-LCP-Public_Senat.png",
                     15 => "http://upload.wikimedia.org/wikipedia/fr/thumb/d/d4/BFM_TV_2004.jpg/120px-BFM_TV_2004.jpg",
                     16 => "http://upload.wikimedia.org/wikipedia/fr/thumb/5/56/Logo_I_tele.png/150px-Logo_I_tele.png",
                     17 => "http://upload.wikimedia.org/wikipedia/fr/a/a6/Direct_Star_logo.png",
                     18 => "http://upload.wikimedia.org/wikipedia/en/thumb/a/a1/Gulli_Logo.png/200px-Gulli_Logo.png",
                     20 => "http://upload.wikimedia.org/wikipedia/fr/8/86/13rue.gif",
                     23 => "http://upload.wikimedia.org/wikipedia/fr/thumb/2/29/Logo-AB1.png/120px-Logo-AB1.png",
                     26 => "http://upload.wikimedia.org/wikipedia/fr/7/7e/ACTION_1996.gif",
                     27 => "http://upload.wikimedia.org/wikipedia/fr/thumb/0/0b/ABMoteurs.jpg/120px-ABMoteurs.jpg",
                     29 => "http://upload.wikimedia.org/wikipedia/fr/5/59/ANIMAUX_1998_BIG.gif",
                     70 => "http://upload.wikimedia.org/wikipedia/fr/thumb/5/52/Demain_TV.jpg/120px-Demain_TV.jpg",
                     83 => "http://upload.wikimedia.org/wikipedia/fr/thumb/c/c4/Equidia.jpg/120px-Equidia.jpg",
                     84 => "http://upload.wikimedia.org/wikipedia/fr/thumb/c/c2/ESCALES_2003.jpg/120px-ESCALES_2003.jpg",
                     87 => "http://upload.wikimedia.org/wikipedia/fr/thumb/9/9e/EuroNews.png/150px-EuroNews.png",
                     89 => "http://upload.wikimedia.org/wikipedia/fr/thumb/4/49/Eurosport_logo.svg/180px-Eurosport_logo.svg.png",
                     119 => "http://upload.wikimedia.org/wikipedia/fr/6/6a/Logo_franceo_2008.png",
                     120 => "http://upload.wikimedia.org/wikipedia/fr/6/6e/Funtv.gif",
                     121 => "http://upload.wikimedia.org/wikipedia/fr/0/04/GameOne2006.PNG",
                     135 => "http://upload.wikimedia.org/wikipedia/fr/thumb/8/8b/LIBERTY_TV_2005.jpg/120px-LIBERTY_TV_2005.jpg",
                     142 => "http://upload.wikimedia.org/wikipedia/fr/thumb/8/80/Mangas.jpg/120px-Mangas.jpg",
                     166 => "http://upload.wikimedia.org/wikipedia/fr/8/84/Nantes7.gif",
                     173 => "http://upload.wikimedia.org/wikipedia/fr/0/0c/Logo_NRJ_Hits.jpg",
                     174 => "http://upload.wikimedia.org/wikipedia/fr/a/ac/Logo_NRJ_Paris.gif",
                     186 => "http://upload.wikimedia.org/wikipedia/fr/d/de/Paris_premiere.jpg",
                     199 => "http://upload.wikimedia.org/wikipedia/fr/thumb/9/9a/RTL9logo.png/120px-RTL9logo.png",
                     206 => "http://upload.wikimedia.org/wikipedia/fr/thumb/d/dc/Tcm.jpg/150px-Tcm.jpg",
                     230 => "http://upload.wikimedia.org/wikipedia/en/thumb/1/1d/PokerChannelEuropeLogo.gif/150px-PokerChannelEuropeLogo.gif",
                     237 => "http://upload.wikimedia.org/wikipedia/fr/thumb/8/83/TV5MONDE.png/200px-TV5MONDE.png",
                     245 => "http://upload.wikimedia.org/wikipedia/fr/c/c1/Logo_TV_Tours.gif",
                     259 => "http://upload.wikimedia.org/wikipedia/en/thumb/9/95/Luxe_TV.png/200px-Luxe_TV.png",
                     268 => "http://upload.wikimedia.org/wikipedia/fr/thumb/2/21/Fashiontv.gif/250px-Fashiontv.gif",
                     288 => "http://upload.wikimedia.org/wikipedia/fr/thumb/c/ce/FRANCE24.svg/100px-FRANCE24.svg.png",
                     294 => "http://upload.wikimedia.org/wikipedia/fr/thumb/6/67/IDF1.png/100px-IDF1.png",
                     1500 => "http://upload.wikimedia.org/wikipedia/fr/f/f7/Logo_nolife.png",
                    );

# defaults values of configuration (command line or config file)
my $cnf_conffile = "$ENV{HOME}/.tv_grab_fr_iphone";
my @cnf_days_offsets = ( 0 );
my $cnf_days_number = 1;
my $cnf_output;
my $cnf_cache;
my $cnf_quiet = 0;
my @cnf_sourceids;
my $cnf_raw = 0;
my $cnf_xmltvid_postfix = ".telerama.fr";
my $cnf_xmltvid_prefix = "";
my $cnf_mythtv_dir = "/var/lib/mythtv";
my $cnf_desc_episode = 0;
my $cnf_desc_crit = 0;
my @cnf_selected_channels = ();
my $cnf_conffile2;              # ?
my $cnf_icondir;                # ?
my $cnf_dst = 1;
my $cnf_debug = 0;

#
# Command line read
# -----------------
my ($cmd_help,$cmd_update_programs,$cmd_update_channels,$cmd_list_channels,$cmd_capabilities,$cmd_description,$cmd_version,$cmd_preferedmethod);
{
    my $rc;
    my @days_offsets;    

    # Command line analysis
    $rc = GetOptions(
                     "description"     => \$cmd_description,
                     "capabilities"    => \$cmd_capabilities,
                     "preferedmethod"  => \$cmd_preferedmethod,
                     "list-channels"   => \$cmd_list_channels,
                     "update-channels" => \$cmd_update_channels,
                     "version"         => \$cmd_version,
                     "help"	       => \$cmd_help,
                     "quiet"           => \$cnf_quiet,
                     "days=i"	       => \$cnf_days_number,
                     "offset:s"	       => \@days_offsets,
                     "config-file:s"   => \$cnf_conffile,
                     "output:s"	       => \$cnf_output,
                     "cache:s"	       => \$cnf_cache,
                     "source:s"        => \@cnf_sourceids,
                     "raw"             => \$cnf_raw,
                     "debug"           => \$cnf_debug,
                    );
    die "Syntax error" if ($rc<1);

    # update programs command is the command by default
    if (! $cmd_help
        && ! $cmd_update_channels
        && ! $cmd_list_channels
        && ! $cmd_capabilities
        && ! $cmd_description
        && ! $cmd_version
        && ! $cmd_preferedmethod) {
        $cmd_update_programs = 1;
    }

    if (@days_offsets) {
        @cnf_days_offsets = split(/,/,join(',',@days_offsets));
    }
    @cnf_sourceids = split(/,/,join(',',@cnf_sourceids));
}

#
# Version (--version)
# =============================
if ($cmd_version) {
    print <<END;
This is $cnf_progname version $main::VERSION
END
    exit(0);
}

#
# Description (--description)
# =============================
if ($cmd_description) {
    # Not really sure what these are for !
    print <<END;
France ($cnf_site_addr)
END
    exit(0);
}

#
# Capabilities (--capabilities)
# =============================
if ($cmd_capabilities) {
    print <<END;
baseline
cache
preferedmethod
END
    exit(0);
}

#
# Prefered method (--preferedmethod)
# ==================================
if ($cmd_preferedmethod) {
    print <<END;
onedayatonce
END
    exit(0);
}

#
# help (--help)
# =============
if ($cmd_help) {
    print <<END;
$cnf_progname: get French television listings in XMLTV format.
Usage:
  $cnf_progname [--days <nb>] [--offset <nb1>[,<nb2,...]] [--source <source_id1>[,<source_id2>,...]]
  $cnf_progname --list-channels
  $cnf_progname --update-channels
  $cnf_progname --capabilities
  $cnf_progname --preferedmethod
  $cnf_progname --description
  $cnf_progname --version
  $cnf_progname --help
Options :
  --days <nb>: number of days to retrieve (default $cnf_days_number).
  --offset <nb1>,<nb2>,...: offset in days since today (default @cnf_days_offsets). Several offset can be set.
  --list-channels: try to generate a xml of channels on stdout
  --capabilities: print some capabilities for mythfilldatabase (useless)
  --description: print the name of this grabber
  --update-channels: try to update directly the icons and the xmlid of the channels in the database.
  --output <file>: Output to file instead of stdout.
  --cache <cachefile>: raw data filename as input (instead of web access) (debug). Must be created with --raw before.
  --config-file <file>: Use specific configuration file.
  --quiet: not implemented (present for compatibility).
  --help: show this help.
Non standard options:
  --source <id1>,<id2>,...: Source identifier list used mythfilldatabase. Several option can be set --> Will call mythfillbase
  --raw: output to stdout in raw format (debug) instead of XML.

Example:
To grab program listings: $cnf_progname [--config-file FILE] [--output FILE] [--days N] [--offset N1,N2,...] [--source SOURCE1,SOURCE2,...]
To list available channels: $cnf_progname --list-channels [--config-file FILE]
To update channels: $cnf_progname --update-channels [--config-file FILE]
To create a cache file (for debugging): $cnf_progname --format raw --output cache.raw
To use the cache file (for debugging): $cnf_progname --cache cache.raw ...
END
    exit(0);
}

#
# Configuration file read 
# -----------------------
my %config;
{
    my ($key,@value);

    open my $config, '<', $cnf_conffile or die "configuration file $cnf_conffile not found";
    while (<$config>) {
        chomp; 
        s/#.*//;                # no comments
        s/^\s+//;               # no leading white
        s/\s+$//;               # no trailing white
        next unless length;     # anything left?

        if (/\=/) {
            ($key,@value) = split /=/, $_;
            $config{$key} = join ',', @value if (@value);
        } elsif ($key) { 
            if (defined($config{$key})) {
                $config{$key} .= ','.$_;
            } else {
                $config{$key} .= $_;
            }
        }
    }
    close($config);

    die "Syntax error in configuration file $cnf_conffile: Channels not found" if (!$config{selected_channels});

    # set parameters from config file
    @cnf_selected_channels = split(/,/,$config{selected_channels});
    @cnf_sourceids = split(/,/,$config{sourceid}) if $config{sourceid};
    $cnf_mythtv_dir = $config{mythtv_dir} if $config{mythtv_dir};
    $cnf_xmltvid_postfix = $config{xmltvid_postfix} if $config{xmltvid_postfix};
    $cnf_xmltvid_prefix = $config{xmltvid_prefix} if $config{xmltvid_prefix};
    $cnf_desc_episode = $config{desc_episode} if $config{desc_episode};
    $cnf_desc_crit = $config{desc_crit} if $config{desc_crit};
    $cnf_dst = $config{dst} if $config{dst};
}

$cnf_conffile2 = "$cnf_mythtv_dir/config.xml"; # ???
$cnf_icondir = "$cnf_mythtv_dir/channels";

# Create output streams
if (@cnf_sourceids) {
    # Source id present=> will create xml file and call mythfilldatabase
    $cnf_output = "/tmp/".$cnf_progfile.".xml";
}

if ($cnf_output) {
    # output redirected to file
    open STDOUT, ">$cnf_output" or die "Can't output to file $cnf_output";
}

#
# list Channels (--list-channels)
# ===============================
if ($cmd_list_channels) {
    my $channels_text;
    my $browser = browser_init();

    $channels_text= browser_getChannels($browser);

    if (!$cnf_raw) {
        print "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n";
        print "<!DOCTYPE tv SYSTEM \"http://membled.com/work/apps/xmltv/cvs_working/xmltv.dtd\">\n";
        print "<tv source-info-url=\"http://$cnf_site_addr/\" source-info-name=\"iPhone\">\n";
        print channel_translate2Xml($channels_text);
        print "</tv>\n";
    } else {
        print $channels_text;
    }
}

#
# Update channels (--update-channels)
# ===================================
if ($cmd_update_channels) {
    my $channels_text;
    my $browser = browser_init();

    $channels_text= browser_getChannels($browser);

    channel_updateMythTvDB($browser, $channels_text);
}

#
# Get programs (default)
# ======================
my %selected_channel;
# List channels to retreive program from
foreach (@cnf_selected_channels) {
    $selected_channel{$_} = 1;
}

if ($cmd_update_programs) {
    my $browser = browser_init();

    if (!$cnf_raw) {
        # Output to STDOUT in XML format or automaticl feeds mythfilldatabase
 
        print "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n";
        print "<!DOCTYPE tv SYSTEM \"xmltv.dtd\">\n";
        print "<tv source-info-url=\"http://$cnf_site_addr/\" source-info-name=\"iPhone\">\n";
    }
     
    # Call for every offsets
    for (my $i=0; $i<=$#cnf_days_offsets; $i++) {
        # Call for every days
        for (my $j=0; $j<=$cnf_days_number-1; $j++) {
            # Get HTML page of TV program
            my $program_text = browser_getPrograms($browser, $cnf_days_offsets[$i]+$j);
            
            printf(STDERR "offset: %d, day: %s\n",$cnf_days_offsets[$i],$j) if ($cnf_debug);

            if (!$cnf_raw) {
                my $txt = program_translate2Xml($cnf_days_offsets[$i]+$j, $program_text);
		print $txt if $txt;
		printf(STDERR "Missing informations for offset: %d, day: %s\n",$cnf_days_offsets[$i],$j) if !$txt;
            } else {
                print $program_text;
            }
        }
    }

    if (!$cnf_raw) {
        print "</tv>\n";
    }

    if (@cnf_sourceids) {
        close STDOUT;
        
        # Call mythfilldatabase for each source
        foreach (@cnf_sourceids) {
            system("mythfilldatabase --file $_ $cnf_output");
        }
        
        exit 0;
    }
}

if ($cnf_output) {
    # close STDOUT if redirected to file
    close STDOUT;
}

exit 0;

#==============================================================================
# Functions
#

# init the Web agent
# global: $browser
# in: none
# out: browser
sub browser_init {
    my $browser = LWP::UserAgent->new(keep_alive => 0,
                                      agent =>$cnf_useragt);
    
    $browser->cookie_jar(HTTP::Cookies->new(file => "$ENV{HOME}/.$cnf_site_addr.cookie"));
    $browser->timeout(10);
    $browser->default_header( [ 'Accept-Language' => "fr-fr"
                                # 'Accept-Encoding' => "gzip,deflate",
                                # 'Accept-Charset' => "ISO-8859-15,utf-8"
                              ] );

    return $browser;
}

# Do http request for a program
# global: $cnf_site_prefix, @cnf_selected_channels, $cnf_cache
# in: browser
# in: offset (offset in days from today)
# out: program_text (HTML text read from server)
sub browser_getPrograms {
    my ($browser,$offset) = @_;
    # date YYYY-MM-DD
    my $date = strftime("%Y-%m-%d", localtime(time()+(24*3600*$offset)) );
    my $url = $cnf_site_prefix.'LitProgrammes1JourneeDetail.php?date='.$date.'&chaines=';

    for (my $i =0 ; $i < @cnf_selected_channels ; $i++ ) {
        $url = $url.$cnf_selected_channels[$i];
        if ($i < (@cnf_selected_channels - 1)) {
            $url = $url.",";
        }
    }

    if (!$cnf_debug) {
        if (!$cnf_cache) {
            # Read from URL
            my $response = $browser->get($url);
            
            die "$url error: ", $response->status_line
                unless $response->is_success;
            
            return $response->content;
        } else {
            # Read from cache file
            my $program_text = "";
            open (FILE, "$cnf_cache") || die "file $cnf_cache not found";
            while (<FILE>) {
                $program_text = $program_text . $_;
            }
            
            return $program_text;
        }
    } else {
        printf(STDERR "URL: $url\n");
        return "";
    }
}


# Retreive channels from URL
# global: $cnf_site_prefix, $browser, $cnf_cache
# in: browser
# in: offset (offset in days from today)
# out: channel (HTML text read from server)
sub browser_getChannels {
  my ($browser, $offset) = @_;
  my $url = $cnf_site_prefix."ListeChaines.php";
  my $channel_text;

  if (!$cnf_debug) {
      if (!$cnf_cache) {
          # Read from URL
          my $response = $browser->get($url);
            
          die "$url error: ", $response->status_line
              unless $response->is_success;
            
          $channel_text = $response->content;
      } else {
          # Read from cache file
          my $program_text = "";
          open (FILE, "<$cnf_cache") || die "file $cnf_cache not found";
          while (<FILE>) {
              $program_text = $program_text . $_;
          }
          
          $channel_text = $program_text;
      }
      
      # add dummy entry for NoLife channel
      $channel_text .= '1500$$$Nolife:$$$:';
  } else {
      $channel_text = "";
      printf(STDERR "URL $url\n");
  }
      
  return $channel_text;
}

# Extract data from program
# global: $cnf_xmltvid_postfix $cnf_xmltvid_prefix
# in: offset (offset in days from today),
# in: html_text (text in HTML read from server)
# out: xml_text (string)
sub program_translate2Xml {
    my ($offset, $html_text) = @_;

    my $xml_text;
  
    # format arrangment
    $html_text =~ tr/\n/ /d;
    $html_text =~ s/:\$\$\$:/\n/g;
    $html_text =~ s/:\$CH\$://g;
    $html_text =~ s/;\$\$\$;//g;

    # text cleanup
    $html_text =~ s/<br \/>/ /g;
    $html_text =~ s/<[A-Za-z\+]>//g;
    $html_text =~ s/<\/[A-Za-z\+]>//g;
    $html_text =~ s/&/&amp;/g;
    $html_text =~ s/</&lt;/g;
    $html_text =~ s/>/&gt;/g;

    Encode::from_to($html_text, "windows-1252", "iso-8859-1");

    my @lines = split("\n",$html_text);

    my %tags = (
                length	   => 'Dur�e :',
                category   => 'Genre :',
                subtitle   => 'Sous-titre :',
                year	   => 'Ann�e :',
                country	   => 'Pays :',
                title_orig => 'Titre original :',
                episode	   => 'Episode :',
                season	   => 'Saison :',
                music	   => 'Musique :',
                image	   => 'Image :',
                scenario   => 'Sc�nario :',
                guests	   => 'Invit�s :',
                presenter  => 'Pr�sentateur :',
                actors	   => 'Acteurs :',
                director   => 'R�alisateur :',
                showview   => 'Showview :',
                stereo	   => 'St�r�o',
                dolbydig   => 'Dolby digital',
                dolby51    => 'Dolby 5.1',
                dolby	   => 'Dolby',
                vmulti     => 'VM',
                caption	   => 'Sous-titr',
                rating	   => 'Interdit aux moins de',
                rating2    => 'Tous publics',
                direct     => 'En direct',
                premiere   => 'In�dit',
                premiere2  => 'Premi�re diffusion',
                lastshow   => 'Rediffusion',
                hdtv	   => 'HD',
                aspect169  => 'En 16/9',
                aspect43   => 'En 4/3'
               );

    # 0: channel id
    # 1: channel name
    # 2: Title
    # 3: date start
    # 4: date stop
    # 5: category
    # 6: description
    # 7: details (duration, ...)
    # 8: rating
    # 9: image presence
    # 10: stars
    # 11: critical
    # 12: airdate
    my %pos = (
               chanid => 0,
               title => 2,
               start => 3,
               stop => 4, 
               category => 5,
               desc => 6,
               details => 7,
               rating => 8,
               image => 9,
               stars => 10,
               crit => 11,
               airdate => 12
              );

    foreach my $line (@lines) {
        my %prog;
        my $date_current;
        my $tmpstring;
        my @tmplist;

        my @prog_data = split('\$\$\$',$line);

        next if (!exists($prog_data[0]));

        $prog{part} = '0';

        # Channel (mandatory)
        # -------------------
        $prog{channel} = $cnf_xmltvid_prefix.$prog_data[$pos{chanid}].$cnf_xmltvid_postfix;

        # Title
        # -----
        $prog{title} = $prog_data[$pos{title}] if ($pos{title});

        # Category
        # --------
        $prog{category}[0] = $prog_data[$pos{category}] if ($pos{category});

        # Description
        # -----------
        $prog{desc} = $prog_data[$pos{desc}] if ($pos{desc});

        # Details: list of informations on program
        # ----------------------------------------
        if ($pos{details}) {
            my $details = $prog_data[$pos{details}]."\$";
            # put tags between infos
            foreach my $tag (%tags) {
                $details =~ s/ *($tag)/\$$1/g;
            }
	
            my @infos = split('\$',$details);

            foreach my $info (@infos) {
                if ($info =~ m/$tags{length} ([0-9]+) min/) {
                    $prog{length} = $1;
                } elsif ($info =~ m/$tags{subtitle} (.*)/) {
                    $prog{subtitle} = $1;
                    # get part from title
                    if ($prog{subtitle} =~ m/ +\(([0-9]+\/[0-9]+)\)/) {
                        @tmplist = split('/',$1);
                        $prog{part} = $tmplist[0];
                        $prog{part_max} = $tmplist[1] if ($tmplist[1]);
                        $prog{subtitle} =~ s/ +\([0-9]+\/[0-9]+\)//;
                    }
                } elsif ($info =~ m/$tags{title_orig} (.*)/) {
                    $prog{title_orig} = $1;
                } elsif ($info =~ m/$tags{category} (.*)/) {
                    $prog{category}[1] = $1;
                } elsif ($info =~ m/$tags{country} (.*)/) {
                    $prog{country} = $1;
                } elsif ($info =~ m/$tags{year} ([0-9]+)/) {
                    $prog{date} = $1;
                } elsif ($info =~ m/$tags{showview} ([0-9]+)/) {
                    $prog{showview} = $1;
                } elsif ($info =~ m/$tags{stereo}/) {
                    $prog{stereo} = 1;
                } elsif ($info =~ m/$tags{dolbydig}/) {
                    $prog{dolby} = "digital";
                } elsif ($info =~ m/$tags{dolby51}/) {
                    $prog{dolby} = "5.1";
                } elsif ($info =~ m/$tags{dolby}/) {
                    $prog{dolby} = "dolby";
                } elsif ($info =~ m/$tags{aspect169}/) {
                    $prog{video_aspect} = "16:9";
                } elsif ($info =~ m/$tags{aspect43}/) {
                    $prog{video_aspect} = "4:3";
                } elsif ($info =~ m/$tags{hdtv}/) {
                    $prog{video_q} = "HDTV";
                } elsif ($info =~ m/$tags{caption}/) {
                    $prog{caption} = 1; # TBC
                } elsif ( ($info =~ m/$tags{premiere}/) || ($info =~ m/$tags{premiere2}/) ) {
                    $prog{premiere} = 1;
                } elsif ($info =~ m/$tags{season} ([0-9\/]*)/) {
                    @tmplist = split('/',$1);
                    $prog{season} = $tmplist[0] if ($tmplist[0]);
                    $prog{season_max} = $tmplist[1] if ($tmplist[1]);
                    printf(STDERR "Season error: $info\n") if (!$tmplist[0]);
                } elsif ($info =~ m/$tags{episode} ([0-9\/]*)/) {
                    @tmplist = split('/',$1);
                    $prog{episode} = $tmplist[0] if ($tmplist[0]);
                    $prog{episode_max} = $tmplist[1] if ($tmplist[1]);
                } elsif ($info =~ m/$tags{lastshow}/) {
                    $prog{lastshow} = 1;
                } elsif ($info =~ m/$tags{rating2}/) {
                    $prog{rating_all} = 1;
                } elsif ($info =~ m/$tags{director} (.*)/) {
                    $prog{director} = [ split(', ',$1) ];
                } elsif ($info =~ m/$tags{presenter} (.*)/) {
                    # Remove (name), "name"
                    $tmpstring = $1;
                    $tmpstring =~ s/ *�.+�//g;
                    $tmpstring =~ s/ *\(.+\) *//g;
                    $prog{presenter} = [ split(', ',$tmpstring) ];
                } elsif ($info =~ m/$tags{guests} (.*)/) {
                    # Remove (name), "name"
                    $tmpstring = $1;
                    $tmpstring =~ s/ *�.+�//g;
                    $tmpstring =~ s/ *\(.+\) *//g;
                    $prog{guest} = [ split(', ',$tmpstring) ];
                } elsif ($info =~ m/$tags{actors} (.*)/) {
                    # Remove (name), "name"
                    $tmpstring = $1;
                    # $tmpstring =~ s/ *�.+�//g;
                    # $tmpstring =~ s/ *\(.+\) *//g;
                    # tmp list -> actors/roles
                    $prog{actor} = [ split(', ',$tmpstring) ];
                }
            }
        }

        # Program rating (in years old)
        # -----------------------------
        if ($prog{rating_all}) {
            $prog{rating_mpaa} = 'G';
            $prog{rating_years} = $prog{rating};
        } elsif ($pos{rating} && $prog_data[$pos{rating}]) {
            if (($prog_data[$pos{rating}] >= 10) && ($prog_data[$pos{rating}] < 13)) {
                $prog{rating_mpaa} = 'PG';
            } elsif (($prog_data[$pos{rating}] >= 13) && ($prog_data[$pos{rating}] < 17)) {
                $prog{rating_mpaa} = 'PG-13';
            } elsif ($prog_data[$pos{rating}] >= 17) {
                $prog{rating_mpaa} = 'R:NC-17';
            } else {
                $prog{rating_mpaa} = 'G';
            }

            $prog{rating_years} = $prog_data[$pos{rating}];
        }

        # Stars rating
        # ------------
        $prog{'star-rating'} = $prog_data[$pos{stars}] if ($pos{stars});
 
        # Critics of the program
        # -----------------------
        $prog{crit} = $prog_data[$pos{crit}] if ($pos{crit});

        # Airdate, start and stop date (mandatory)
        # ----------------------------------------
        # airdate: date, start/stop: hours
        my @date = split('/', $prog_data[$pos{airdate}]);
	
        $date_current = $date[2].$date[1].$date[0] if ($date[0] && $date[1] && $date[2]);

        # Start time
        my $hstart = $prog_data[$pos{start}];
        $hstart =~ tr/$://d;
        my $tz_offset = get_tz($date[0], $date[1], $date[2], $hstart);

        $prog{start} = $date_current.$hstart." ".$tz_offset;

        # Stop time
        my $hstop = $prog_data[$pos{stop}];
        $hstop =~ tr/$://d;

        if ($hstart < $hstop) {
            # same day
            $tz_offset = get_tz($date[0], $date[1], $date[2], $hstop);

            $prog{stop} = $date_current.$hstop." ".$tz_offset;
        } elsif ($hstart >=  $hstop) {
            # next day or no duration
            my @time = split(':', $prog_data[$pos{stop}]);
            my $dt = DateTime->new(year => $date[2],
                                   month => $date[1],
                                   day => $date[0],
                                   hour => $time[0],
                                   minute => $time[1],
                                   second => $time[2] );
            if ($hstart > $hstop) {
                # next day
                $dt->add( days => 1 );
            } else {
                # add duration
                $dt->add( minutes => $prog{length});
            }      
     
            $tz_offset = get_tz($dt->day, $dt->month, $dt->year, $hstop);

            $prog{stop} = $dt->strftime("%Y%m%d%H%M%S ".$tz_offset);
        }
         
        # Production date
        # ---------------
        # current date if no date of production
        $prog{date} = $date_current if (!$prog{date});
	
        # Pictures
        # --------
        if ($prog_data[$pos{image}] != 0) {
            my @date = split('/', $prog_data[$pos{airdate}]);
            my @time = split(':', $prog_data[$pos{start}]);
            $prog{image} = $date[2]."-".$date[1]."-".$date[0]."_".$prog_data[$pos{chanid}]."_".$time[0].":".$time[1].".jpg";
        }
	
        # output XML program
        # ------------------
        if ($prog{start} && $prog{stop} && $prog{channel}) {
            $xml_text .= sprintf(" <programme start=\"%s\" stop=\"%s\" channel=\"%s\"",,$prog{start}, $prog{stop}, $prog{channel});
            $xml_text .= sprintf(" showview=\"%s\"", $prog{showview}) if $prog{showview};
            $xml_text .= sprintf(">\n");
            $xml_text .= sprintf("  <title lang=\"fr\">%s</title>\n", $prog{title});
            $xml_text .= sprintf("  <sub-title lang=\"fr\">%s</sub-title>\n", $prog{subtitle}) if $prog{subtitle};
            $xml_text .= sprintf("  <desc lang=\"fr\">");
            if ($cnf_desc_episode && ($prog{season} || $prog{episode} || ($prog{part} ne '0'))) {
                # Note: Mythtv misunderstand programs numberring from 0 (so give real episodes numbers)
                $xml_text .= sprintf("Saison %02d",$prog{season}) if ($prog{season});
                $xml_text .= sprintf("/%02d",$prog{season_max}) if ($prog{season_max});
                $xml_text .= sprintf(" Episode ");
                $xml_text .= sprintf("%02d",$prog{episode}) if ($prog{episode});
                $xml_text .= sprintf("/%02d",$prog{episode_max}) if ($prog{episode_max});
                $xml_text .= sprintf(" ");
                $xml_text .= sprintf("%02d",$prog{part}) if ($prog{part});
                $xml_text .= sprintf("/%02d",$prog{part_max}) if ($prog{part_max});
                $xml_text .= sprintf("\n");
            }
            $xml_text .= sprintf("%s",$prog{desc});
            if ($cnf_desc_crit) {
                $xml_text .= sprintf(" Critique: %s", $prog{crit}) if ($prog{crit});
            }
            $xml_text .= sprintf("</desc>\n");
            $xml_text .= sprintf("  <date>%s</date>\n",$prog{date});
            # Mythtv read only 1st one
            $xml_text .= sprintf("  <category lang=\"fr\">%s</category>\n", $prog{category}[0]) if $prog{category}[0];
            $xml_text .= sprintf("  <category lang=\"fr\">%s</category>\n", $prog{category}[1]) if $prog{category}[1];
            $xml_text .= sprintf("  <length units=\"minutes\">%s</length>\n", $prog{length}) if $prog{length};
            $xml_text .= sprintf("  <country>%s</country>\n", $prog{country}) if $prog{country};
            if ($prog{guest} || $prog{actor} || $prog{director} || $prog{presenter} ) {
                $xml_text .= sprintf("  <credits>");
                foreach my $i (0..$#{$prog{presenter}}) {
                    $xml_text .= sprintf("<presenter>%s</presenter>", $prog{presenter}[$i]);
                }
                foreach my $i (0..$#{$prog{guest}}) {
                    $xml_text .= sprintf("<guest>%s</guest>", $prog{guest}[$i]);
                }
                foreach my $i (0..$#{$prog{director}}) {
                    $xml_text .= sprintf("<director>%s</director>",$prog{director}[$i]);
                }
                foreach my $i (0..$#{$prog{actor}}) {
                    $xml_text .= sprintf("<actor>%s</actor>", $prog{actor}[$i]);
                }
                $xml_text .= sprintf("</credits>\n");
            }
            $xml_text .= sprintf("  <star-rating>%s/5</star-rating>\n", $prog{'star-rating'}) if $prog{'star-rating'};
            $xml_text .= sprintf("  <rating system=\"MPAA\">%s</rating>\n", $prog{rating_mpaa}) if $prog{rating_mpaa};
            $xml_text .= sprintf("  <rating system=\"years\">%s</rating>\n", $prog{rating_years}) if $prog{rating_years};
            $xml_text .= sprintf("  <premiere />\n") if ($prog{premiere});
            if ($prog{season} || $prog{episode} || ($prog{part} ne '0')) {
                $xml_text .= sprintf("  <episode-num system=\"xmltv_ns\">");
                $xml_text .= sprintf("%s", $prog{season}-1) if ($prog{season});
                $xml_text .= sprintf("/%s", $prog{season_max}) if ($prog{season_max});
                $xml_text .= sprintf(".");
                $xml_text .= sprintf("%s",$prog{episode}-1) if ($prog{episode});
                $xml_text .= sprintf("/%s", $prog{episode_max}) if ($prog{episode_max});
                $xml_text .= sprintf(".");
                $xml_text .= sprintf("%s",$prog{part}-1) if ($prog{part});
                $xml_text .= sprintf("/%s", $prog{part_max}) if ($prog{part_max});
                $xml_text .= sprintf("</episode-num>\n");
            }
            if ($prog{video_aspect} || $prog{video_q}) {
                $xml_text .= sprintf("  <video>");
                $xml_text .= sprintf("<aspect>%s</aspect>",$prog{video_aspect}) if $prog{video_aspect};
                $xml_text .= sprintf("<quality>%s</quality>",$prog{video_q}) if $prog{video_q};
                $xml_text .= sprintf("</video>\n");
            }
            # strange tagging for Mythtv
            if ($prog{stereo} || $prog{dolby}) {
                $xml_text .= sprintf("  <audio>");
                if ($prog{stereo}) {
                    $xml_text .= sprintf("<stereo>stereo</stereo>");
                } else {
                    $xml_text .= sprintf("<stereo>mono</stereo>");
                }
                if ($prog{dolby}) {
                    $xml_text .= sprintf("<stereo>dolby</stereo>") if ($prog{dolby});
                    $xml_text .= sprintf("<stereo>surround</stereo>") if ($prog{dolby} eq "5.1");
                    $xml_text .= sprintf("<stereo>dolby digital</stereo>") if ($prog{dolby} eq "digital");
                }
                $xml_text .= sprintf("</audio>\n");
            }
            $xml_text .= sprintf("  <subtitle type=\"teletext\" />\n") if $prog{caption};
            $xml_text .= sprintf("  <previously-shown />\n") if $prog{lastshow};
            $xml_text .= sprintf("  <icon src=\"%s%s\" />\n",$cnf_site_img,$prog{image}) if $prog{image};
            $xml_text .= sprintf(" </programme>\n");
        } else {
            die "error in file at line $line\n\n";
        }
    }

    return $xml_text;
}

# Return the timezone offset (Only work for DST on france)
# global: $cnf_dst
# in: day,month,year (datetime)
# in: h (hour string 235959)
# out: tz (timezone string +0100)
sub get_tz {
    my ($day,$month,$year,$h) = @_;
    my $tz;

    if ($cnf_dst) {         
        # TODO: compute tz for all countries instead of only France
        # detect last sunday of October/March of the year at 02:00
        my $oct_day = UnixDate("last sunday in october $year", "%d");	
        my $mar_day = UnixDate("last sunday in march $year", "%d"); 
        if ( (($month == 10) && ($day == $oct_day) && ($h >= "020000"))
             || (($month == 10) && ($day > $oct_day))
             || ($month > 10)
             || (($month == 3) && ($day == $mar_day) && ($h < "020000"))
             || (($month == 3) && ($day < $mar_day))
             || ($month < 3) ) {
            # winter: current time has 1hour less
            $tz = "+0100";
        } else {
            # summer
            $tz = "+0200";
        }
    } else {
        $tz = strftime("%z", localtime);
    }

    return $tz;
}


# Generate channels in XML
# global: $cnf_xmltvid_prefix $cnf_xmltvid_postfix
# in: html_text (text in HTML read from server)
# out: none
sub channel_translate2Xml {
    my $html_text = shift;

    Encode::from_to($html_text, "windows-1252", "iso-8859-1");

    my @lines = split(/:\$\$\$:/,$html_text);
    foreach (@lines) {
        my ($chaine,$nom) = split('\$\$\$');
        my $icon = $cnf_icon_urls{$chaine};
        print <<END;
  <channel id="$cnf_xmltvid_prefix$chaine$cnf_xmltvid_postfix">
	<display-name>$nom</display-name>
END
        print "	<icon src=\"$icon\" />\n" if ($icon);
        print "  </channel>\n";
    }
}

# Update channels
# global: $cnf_icon_dir,$cnf_conffile2
# in: browser
# in: html_text (text in HTML read from server)
# out: none
sub channel_updateMythTvDB {
    my ($browser,$html_text) = @_;
    my $xml = new XML::Simple;

    # create directory to store channels in MythTV directory
    mkdir($cnf_icondir,0777) if (! -d $cnf_icondir);
    chdir($cnf_icondir);

    # Connect to MySQL MythTV (from where comes this config file ?)
    my $data = $xml->XMLin("$cnf_conffile2") || die "can't read $cnf_conffile2\n";
    my $db = $data->{UPnP}->{MythFrontend}->{DefaultBackend};
    my $dsn = "DBI:mysql:database=$db->{DBName};host=$db->{DBHostName}";
    my $dbh = DBI->connect($dsn, $db->{DBUserName}, $db->{DBPassword});
    die "pas de cxion � la bdd\n" if (!$dbh);

    Encode::from_to($html_text, "windows-1252", "iso-8859-1");
    my @lines = split(/:\$\$\$:/,$html_text);

    my $sql = "select chanid, name, sourceid from channel";
    my $sth = $dbh->prepare($sql) || die "$sql\n";
    $sth->execute();
    while (my ($id,$name,$source) = $sth->fetchrow()) {
        $name =~ s/ *$//;

        foreach (@lines) {
            my ($chaine,$nom) = split('\$\$\$');
            next if (!$nom);
            $nom = $nom;
            my $icon = $cnf_icon_urls{$chaine};
            my $idx = index(lc($name),lc($nom));
            my $match = 0;
            if (lc($name) eq lc($nom)) {
                print "Exact match $name\n";
                $match = 1;
            } elsif ($idx == 0 && substr($name,length($nom),1) eq " ") {
                my $suffixe = substr($name,length($nom)+1);
                if ($suffixe =~ /^(\(|HD)/) {
                    print "found $nom for $name\n";
                    $match = 1;
                }
            }
            if ($match) {
                if ($icon) {
                    my $fich = $icon;
                    $fich =~ s/^.+\///;

                    if (!-f $fich) {
                        my $response = $browser->get($icon);

                        if (!$response->is_success) {
                            sleep(3);
                            my $response = $browser->get($icon);
                        }

                        die "$icon error: ", $response->status_line
                            unless $response->is_success;

                        open(F,">$fich") || die "can't write $icon to directory $cnf_icondir\n";
                        print F $response->content;
                        close(F);
                    }
                    $icon = "$cnf_icondir/$fich";
                }

                $sql = "update channel set ";
                $sql .= "icon = \"$icon\", " if ($icon);
                $sql .= "xmltvid = \"".$cnf_xmltvid_prefix.$chaine.$cnf_xmltvid_postfix."\" where chanid = $id and sourceid = $source";

                $dbh->do($sql) || die($sql);
            } else {
                if ($name =~ /^RTL9/) {
                    $name = "RTL 9"; redo;
                } elsif ($name =~ /^Luxe/) {
                    $name = "Luxe TV";
                } elsif ($name =~ /^AB 1/) {
                    $name = "AB1"; redo;
                } elsif ($name eq "IDF 1") {
                    $name = "IDF1"; redo;
                } elsif ($name eq "i>TELE" || $name =~ /^i\> TELE/) {
                    $name = "iT�l�"; redo;
                } elsif ($name eq "TV5 Monde") {
                    $name = "TV5MONDE"; redo;
                } elsif ($name =~ /Arte /) { 
                    $name = "Arte"; redo;
                } elsif ($name =~ /^DirectStar/i) {
                    $name = "Direct Star"; redo;
                } elsif ($name eq "France O" || $name eq "France �") {
                    $name = "France �"; redo;
                } elsif ($name eq "Demain.tv") {
                    $name = "Demain.TV"; redo;
                } elsif ($name eq "T�l�nantes Nantes 7" || $name eq "NANTES") {
                    $name = "Nantes 7"; redo;
                } elsif ($name eq "NRJ12") {
                    $name = "NRJ 12"; redo;
                } elsif ($name eq "LCP") {
                    $name = "La Cha�ne Parlementaire"; redo;
                } elsif (lc($name) eq "paris premiere") {
                    $name = "Paris Premi�re"; redo;
                } elsif ($name =~ /^TLM/) {
                    $name = "TLM - T�l� Lyon M�tropole";
                } elsif ($name eq "M6HD") {
                    $name = "M6"; redo;
                }
            }
        }
    }

    $sth->finish();
    $dbh->disconnect();
}



