#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Created on 18 nov. 2012

@author: franck
'''

import socket
from xbmcudpevent.XBMCUDPMsgInterpreter import XBMCListener,\
    XBMCUDPMsgInterpreter
import logging
import logging.config
import louie
import os
from xbmcrequest.xbmcrequest import XbmcRequest
import serial
import json
import urllib2
from collections import Counter
import time

# create logger
logging.config.fileConfig(os.path.dirname(os.path.abspath(__file__)) + "/logging.conf")
logger = logging.getLogger("leoagent")


profilManager = "http://localhost:8000/tvprogram/rest/scoring/"
profilManager = "http://leomedia.herokuapp.com/tvprogram/rest/scoring/"
profilheaders = {"Content-Type":"text/plain"}

def scoreOnProfile(profile_id, request):
        """
        Get scoreOnProfile score
        @param profile_id: 
        @param request: the text to evaluate
        """
        request = json.dumps(request)
        req = urllib2.Request(profilManager+profile_id, request,profilheaders)
        response = urllib2.urlopen(req)
        return json.loads(response.read())
    
LAMP_ON='4;'
LAMP_OFF='5;'
LAMP_RED='6;'
LAMP_GREEN='7;'
LAMP_BLUE='8;'
LAMP_WHITE='9;'
LAMP_STATE='10;'
LAMP_UPINTENSITY='11;'
LAMP_DOWNINTENSITY='12;'

LAMP_MSGTONAME = {
                  LAMP_ON :'On',
                  LAMP_OFF:'Off',
                  LAMP_RED : 'Red',
                  LAMP_GREEN : 'Green',
                  LAMP_BLUE : 'Blue',
                  LAMP_WHITE : 'White',
                  LAMP_STATE : 'Status',
                  LAMP_UPINTENSITY : 'Up intensity',
                  LAMP_DOWNINTENSITY : 'Down intensity',
                  }
profileToColorMessage = {'Rouge': LAMP_RED,
                          'Vert':  LAMP_GREEN,
                          'Bleu':  LAMP_BLUE,
                          'Blanc': LAMP_WHITE,
                          
                          'Techno': LAMP_RED,
                          'Rock':  LAMP_GREEN,
                          'Pop':  LAMP_BLUE,
                          'Musique diverse': LAMP_WHITE}
    
class LEOXBMCListener(XBMCListener):
    def __init__(self):
        logger.debug("New LEOXBMC listener")
        XBMCListener.__init__(self)
        #super(LEOXBMCListener, self).__init__()
        self.xbmcIP=None
        self.xbmcrequest=XbmcRequest(None)
        self.irLamp=None
        self.lastColor = None
        self.lastMediaChanged = None # Anti rebound
        
    def xbmcAddress(self):  
        return "http://%s:8080/jsonrpc" % self.xbmcIP
    
    def setxmbcadress(self):
        self.xbmcrequest.xbmc=self.xbmcAddress()
        
    def __del__(self):
        logger.debug("XBMC Listener ending")
                        
    def sendToLamp(self,msg):
        self.irLamp.write(msg)
        time.sleep(.1)
        
    def OnPlayBackPaused(self,args):
        self.irLamp.write(LAMP_OFF)

    def OnPlayBackResumed(self,args):
        self.irLamp.write(LAMP_ON)
    
    def MediaChanged(self,args):
        logger.debug('MediaChanged %s ' % args)
        if args == self.lastMediaChanged:
            return None
        self.lastMediaChanged=args
        # Searching active player
        activePlayer = self.xbmcrequest.PlayerGetActivePlayers()
        playerid=activePlayer['result'][0]['playerid']
        mediatype=activePlayer['result'][0]['type']
        logger.debug("player %s %s " % (playerid,mediatype))
        # Get media info
        mediainfo=self.xbmcrequest.PlayerGetItem(playerid=playerid)
        logger.debug("media Info %s" % mediainfo)

        info_to_test=[] 
        profile="2"
        if mediatype=='audio':
            info_to_test=["genre","artist", "title",]# "year"]
            profile="3"
        elif mediatype=='video':
            info_to_test=["genre", "title","label", "plot"]
            profile="2"

        info=mediainfo["result"]["item"]
        request=" ".join([info[val].encode('utf-8') for val in info_to_test]).lower()
        logger.debug("Request for scoreOnProfile : %s" % request)
        res=scoreOnProfile(profile,request)
        logger.debug("prof : %s " % res)
        scoring = Counter(dict([(vignette, sum(c.values())) for vignette,c in res]))
        logger.debug("Scoring : %s " % scoring)

        if scoring:
            color, _ = (scoring.most_common()[0])
            logger.debug("Send to arduino %s" % LAMP_MSGTONAME[profileToColorMessage[color]])
            self.sendToLamp(profileToColorMessage[color])
            self.setIntensity(color)

    def setIntensity(self,color):
        if color == self.lastColor:
            self.sendToLamp(LAMP_UPINTENSITY)
        else:
            # Downcolor at min
            logger.debug("reset intensity")
            for i in range(3):
                self.sendToLamp(LAMP_DOWNINTENSITY)
                logger.debug(i)
            self.lastColor = color
        
    

def main():
    my_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    my_socket.bind(('',8278))

    print 'start service ...'
    trace = LEOXBMCListener()
    interpeter = XBMCUDPMsgInterpreter()
    
    ser = serial.Serial('/dev/ttyACM0', baudrate=9600,timeout=.2)
    trace.irLamp=ser
    
    # init 
    ser.setDTR( level=False ) # set the reset signal
    time.sleep(2)             # wait two seconds, an Arduino needs some time to really reset
                              # don't do anything here which might overwrite the Arduino's program
    ser.setDTR( level=True )  # remove the reset signal, the Arduino will restart
    time.sleep(3)
    
    logger.info("Test colors")   
    for color in [LAMP_RED, LAMP_GREEN, LAMP_BLUE, LAMP_WHITE]:
        logger.debug('  test %s ' % color) 
        trace.sendToLamp(color)
        time.sleep(0.4)

    logger.info("Test up intensity")   
    for intensity in range(4):
        logger.debug('  test %s ' % intensity) 
        trace.sendToLamp(LAMP_UPINTENSITY)
        time.sleep(0.1)

    logger.info("Test down intensity")   
    for intensity in range(4):
        logger.debug('  test %s ' % intensity) 
        trace.sendToLamp(LAMP_DOWNINTENSITY)
        time.sleep(0.2)

    trace.sendToLamp(LAMP_OFF)
    
    while True :
        message , (address, sock)  = my_socket.recvfrom(8192)
        print address
        print message
        trace.xbmcIP=address
        trace.setxmbcadress()
        interpeter.parseMsg(message)

if __name__ == "__main__" :
    main()
    
