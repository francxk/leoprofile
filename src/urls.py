from django.conf.urls import patterns, include, url
import logging
from django.contrib import admin
import settings

# Get an instance of a logger
logger = logging.getLogger('settings')

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'LEOGoals.views.home', name='home'),
    # url(r'^LEOGoals/', include('LEOGoals.foo.urls')),

    # LEO Management
    url(r'^leogoals/', include('LEOGoals.urls')),

    # TV Program Management
    url(r'^tvprogram/', include('tv_program.urls')),

    # Connection to LEO
    url(r'^leo/', include('leo.urls')),
    
    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    # Admin grappelli
    url(r'^grappelli/', include('grappelli.urls')),

    # Auto suggest tag
    (r'^taggit_autosuggest/', include('taggit_autosuggest.urls')),
    
    # Serve user uploaded files (imageField)
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
        }),
)
