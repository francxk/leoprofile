# -*- coding: utf-8 -*-
'''
Created on 11 oct. 2012

@author: franck
'''
import logging
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
import time
import serial
import json

logger = logging.getLogger('tvprogram.view')

LAMP_ON='4;'
LAMP_OFF='5;'
LAMP_RED='6;'
LAMP_GREEN='7;'
LAMP_BLUE='8;'
LAMP_WHITE='9;'
LAMP_STATE='10;'
LAMP_UPINTENSITY='11;'
LAMP_DOWNINTENSITY='12;'

LAMP_MSGTONAME = {
                  LAMP_ON :'On',
                  LAMP_OFF:'Off',
                  LAMP_RED : 'Red',
                  LAMP_GREEN : 'Green',
                  LAMP_BLUE : 'Blue',
                  LAMP_WHITE : 'White',
                  LAMP_STATE : 'Status',
                  LAMP_UPINTENSITY : 'Up intensity',
                  LAMP_DOWNINTENSITY : 'Down intensity',
                  }
themeToColorMessage = {
                        'violence': LAMP_RED,
                        'documentaire':  LAMP_GREEN,
                        'reflexion':  LAMP_BLUE,
                        'actualite': LAMP_WHITE,
                          
                        'dessinanime': LAMP_RED, #Rose
                        'sport':  LAMP_GREEN, # Orange
                        'religion':  LAMP_BLUE, #pourpre
                        'connaissance': LAMP_WHITE, #jaune
                        }

logger.debug("Init arduino -------------------------------------------")
leobox = serial.Serial('/dev/ttyACM0', baudrate=9600,timeout=.2)

# init 
leobox.setDTR( level=False ) # set the reset signal
time.sleep(2)             # wait two seconds, an Arduino needs some time to really reset
                          # don't do anything here which might overwrite the Arduino's program
leobox.setDTR( level=True )  # remove the reset signal, the Arduino will restart
time.sleep(3)


class SetColor(APIView):
    """
    ###Set Color of LEO (Box)
    The color is chosen according theme parameter
    """    
    def get(self, request):
        logger.debug("Set color of LEO %s" % request.QUERY_PARAMS)
        themes = request.QUERY_PARAMS['theme']
        
        if 'actualite' in themes:
            self.sendToLamp(themeToColorMessage['actualite'])
        elif 'reflexion' in themes:
            self.sendToLamp(themeToColorMessage['reflexion'])
        elif 'documentaire' in themes:
            self.sendToLamp(themeToColorMessage['documentaire'])
        elif 'violence' in themes:
            self.sendToLamp(themeToColorMessage['violence'])
        elif 'dessinanime' in themes:
            self.sendToLamp(themeToColorMessage['dessinanime'])
        elif 'sport' in themes:
            self.sendToLamp(themeToColorMessage['sport'])
        elif 'religion' in themes:
            self.sendToLamp(themeToColorMessage['religion'])
        elif 'connaissance' in themes:
            self.sendToLamp(themeToColorMessage['connaissance'])
        
        return Response(status=status.HTTP_200_OK)

    def sendToLamp(self,msg):
        logger.debug("Send %s to LEO" % LAMP_MSGTONAME[msg])
        leobox.write(msg)
        time.sleep(.1)