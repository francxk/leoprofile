from django.conf.urls import patterns, include, url
import logging

# Get an instance of a logger
logger = logging.getLogger('settings')

urlpatterns = patterns('LEOGoals.views',
    # Examples:
    # url(r'^$', 'django_home_user_mgnt.views.home', name='home'),
    # url(r'^django_home_user_mgnt/', include('django_home_user_mgnt.foo.urls')),
    url(r'vignettesort/(?P<vignette_id>[^/]+)/$', 'profilevignettesort.profilevignettesort'),
    url(r'vignettesort/(?P<vignette_id>[^/]+)/newsorting/$', 'profilevignettesort.newsorting'),
    
)

#Start Application
