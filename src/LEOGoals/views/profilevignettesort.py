'''
Created on 27 oct. 2012

@author: franck
'''
import datetime
from django.http import HttpResponse
import logging
from django.shortcuts import render_to_response
from LEOGoals.models import OrderedVignetteSet, Vignette
import json
from LEOGoals.models.OrderedVignetteSet import VignetteRank

logger = logging.getLogger('leo.view')

def profilevignettesort(request, vignette_id):
    logger.debug("sorting profile %s" % vignette_id)
    
    vignette_set = OrderedVignetteSet.objects.get(id=vignette_id)
    vignettes = vignette_set.vignettes.order_by('vignetterank')

    return render_to_response('LEOGoals/orderedvignetteset_sort.html',
                              {'vignettes':vignettes, 'vignette_set' : vignette_set})
    
def newsorting(request, vignette_id):
    logger.debug("New sorting for %s" % vignette_id)
    photos = request.POST.getlist('photo[]')
    vignette_set = OrderedVignetteSet.objects.get(id=vignette_id)

    # cleaning relation
    VignetteRank.objects.filter(orderedVignetteSet=vignette_set).delete()
    for pos, photo in enumerate(photos):
        VignetteRank.objects.create(rank=pos, vignette=Vignette.objects.get(id=photo),
                                    orderedVignetteSet=vignette_set)
    return HttpResponse(status=200)