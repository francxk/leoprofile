#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file has been automatically generated, changes may be lost if you
# go and generate it again. It was generated with the following command:
# manage.py dumpscript LEOGoals

import datetime
from decimal import Decimal
from django.contrib.contenttypes.models import ContentType

def run():
    from LEOGoals.models.UserDesc import UserDesc

    LEOGoals_userdesc_1 = UserDesc()
    LEOGoals_userdesc_1.name = u'Franck'
    LEOGoals_userdesc_1.forename = u't'
    LEOGoals_userdesc_1.birthdate = None
    LEOGoals_userdesc_1.comments = u''
    LEOGoals_userdesc_1.suspended = False
    LEOGoals_userdesc_1.save()

    from LEOGoals.models.Credential import Credential


    from LEOGoals.models.OrderedVignetteSet import VignetteRank

    LEOGoals_vignetterank_1 = VignetteRank()
    LEOGoals_vignetterank_1.rank = 0
    LEOGoals_vignetterank_1.save()

    LEOGoals_vignetterank_2 = VignetteRank()
    LEOGoals_vignetterank_2.rank = 1
    LEOGoals_vignetterank_2.save()

    LEOGoals_vignetterank_3 = VignetteRank()
    LEOGoals_vignetterank_3.rank = 2
    LEOGoals_vignetterank_3.save()

    LEOGoals_vignetterank_4 = VignetteRank()
    LEOGoals_vignetterank_4.rank = 3
    LEOGoals_vignetterank_4.save()

    from LEOGoals.models.Vignette import Vignette

    LEOGoals_vignette_1 = Vignette()
    LEOGoals_vignette_1.title = u'horreur'
    LEOGoals_vignette_1.image = u'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQcs6qSR69859KvmyViZnzQyNGNUTNIGRn0OVTJJtLk6wvkk9MEYw'
    LEOGoals_vignette_1.save()

    LEOGoals_vignette_2 = Vignette()
    LEOGoals_vignette_2.title = u'action'
    LEOGoals_vignette_2.image = u'https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcRmCeULE_HFpH_EOivgu0SP1rAyaEQT-leZol6kX8yvAKzz8R7T'
    LEOGoals_vignette_2.save()

    LEOGoals_vignette_3 = Vignette()
    LEOGoals_vignette_3.title = u'bd'
    LEOGoals_vignette_3.image = u'https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTtWEmuw9eNKtD-aa_fxGk262bSimvr1f10V-F3nq5ruGTpLCQgnw'
    LEOGoals_vignette_3.save()

    LEOGoals_vignette_4 = Vignette()
    LEOGoals_vignette_4.title = u'leo'
    LEOGoals_vignette_4.image = u'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSfGbqRMb5IvaV1F_9iocbnhVmNq8AMYHM_q6ZixVwW_irMw0PT'
    LEOGoals_vignette_4.save()

    from LEOGoals.models.OrderedVignetteSet import OrderedVignetteSet

    LEOGoals_orderedvignetteset_1 = OrderedVignetteSet()
    LEOGoals_orderedvignetteset_1.title = u'Profile soir'
    LEOGoals_orderedvignetteset_1.save()

    LEOGoals_orderedvignetteset_1.vignettes.add(LEOGoals_vignette_4)
    LEOGoals_orderedvignetteset_1.vignettes.add(LEOGoals_vignette_1)
    LEOGoals_orderedvignetteset_1.vignettes.add(LEOGoals_vignette_3)
    LEOGoals_orderedvignetteset_1.vignettes.add(LEOGoals_vignette_2)

    LEOGoals_vignetterank_1.vignette = LEOGoals_vignette_4
    LEOGoals_vignetterank_1.orderedVignetteSet = LEOGoals_orderedvignetteset_1
    LEOGoals_vignetterank_1.save()

    LEOGoals_vignetterank_2.vignette = LEOGoals_vignette_1
    LEOGoals_vignetterank_2.orderedVignetteSet = LEOGoals_orderedvignetteset_1
    LEOGoals_vignetterank_2.save()

    LEOGoals_vignetterank_3.vignette = LEOGoals_vignette_3
    LEOGoals_vignetterank_3.orderedVignetteSet = LEOGoals_orderedvignetteset_1
    LEOGoals_vignetterank_3.save()

    LEOGoals_vignetterank_4.vignette = LEOGoals_vignette_2
    LEOGoals_vignetterank_4.orderedVignetteSet = LEOGoals_orderedvignetteset_1
    LEOGoals_vignetterank_4.save()






