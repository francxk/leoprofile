'''
Created on 27 oct. 2012

@author: franck
'''
from django.utils.translation import ugettext_lazy as _

import logging
from django.db import models
from LEOGoals.models.Vignette import Vignette
from django_extensions.management.commands.sqldiff import ORDERING_FIELD

# Get an instance of a logger
logger = logging.getLogger('model')


    
class OrderedVignetteSet(models.Model):
    '''
    Ordered Vignette Set.
    '''
    help_text = _(u'Ordered Vignette Set')
        
    def __init__(self, *args, **kwargs):
        super(OrderedVignetteSet, self).__init__(*args, **kwargs)

    
    title = models.CharField(_(u"Set Title"),max_length=35,)
    vignettes = models.ManyToManyField(Vignette, through='VignetteRank')

    def __unicode__(self):
        return self.title
    
    # Cela semble obligatoire pour la generation de la base
    class Meta:
        app_label='LEOGoals'


class VignetteRank(models.Model):
    '''
    Technical class for ordering components of OrderedVignetteSet
    '''
    vignette = models.ForeignKey(Vignette, null=True)
    orderedVignetteSet = models.ForeignKey(OrderedVignetteSet, null=True)
    rank = models.IntegerField()
    # Cela semble obligatoire pour la generation de la base
    class Meta:
        app_label='LEOGoals'
        ordering= ('rank',)