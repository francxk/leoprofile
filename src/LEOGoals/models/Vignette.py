'''
Created on 21 oct. 2012

@author: franck
'''
from django.utils.translation import ugettext_lazy as _

import logging
from django.db import models
from taggit_autosuggest.managers import TaggableManager

# Get an instance of a logger
logger = logging.getLogger('model')

class Vignette(models.Model):
    '''
    Vignette (with picture), that materialize a mood.
    '''
    help_text = _(u'Vignette to exprime a mood')
    
    tags = TaggableManager(help_text=_(u"Keywords/tags associated to vignette.(Comma separated)"),
                           blank=True)
    
    def __init__(self, *args, **kwargs):
        super(Vignette, self).__init__(*args, **kwargs)

    
    title = models.CharField(_(u"Title"),max_length=35,)

    image = models.URLField(_("Thumbnail url"),
                              null=True, blank=True)

    description = models.TextField(_(u"Description"),)

    def __unicode__(self):
        return self.title
    
    # Cela semble obligatoire pour la generation de la base
    class Meta:
        app_label='LEOGoals'
