'''
Created on 5 mai 2012

@author: franck
'''
from django.db import models

from django.utils.translation import ugettext_lazy as _
import logging

# Get an instance of a logger
logger = logging.getLogger('model')


class UserDesc(models.Model):
    '''
    User description : This is identity attributes
    '''


    name=models.CharField(_(u"Name of the person"), max_length=50)
    forename=models.CharField(_(u"Forename of the person"), max_length=50)

    birthdate=models.DateField(_(u"Birth Date"), blank=True, null=True)

    comments=models.CharField(_(u"Comments"), max_length=50, blank=True, null=True)
 
    suspended=models.BooleanField(_(u"person suspended"), default=False)
    
    def __init__(self, *args, **kwargs):
        super(UserDesc, self).__init__(*args, **kwargs) 
        
    def __unicode__(self):
        return self.name + " " + self.forename
    
    # Cela semble obligatoire pour la generation de la base
    class Meta:
        app_label='LEOGoals'
        verbose_name = "Person Description"
