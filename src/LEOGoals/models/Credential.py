'''
Created on 5 mai 2012

@author: franck
'''
from django.db import models

from django.utils.translation import ugettext_lazy as _
from LEOGoals.models.UserDesc import UserDesc
import logging

# Get an instance of a logger
logger = logging.getLogger('model')

#creation de la classe Credential

class Credential(models.Model):
    '''
    An id to identify a person
    '''
    
    idCred=models.CharField(_(u"Identifier of a person"),
                            max_length=50,
                            unique=True)
    source=models.CharField(_(u"Source of the identifier"), max_length=50)
    lastUsed=models.DateTimeField(_(u"Last time credential used"),
                                  blank=True, null=True
                                  )
    person = models.ForeignKey(UserDesc, verbose_name = _(u"Person attach to the identifier"),blank=True, null=True, on_delete=models.SET_NULL)
    
    def __unicode__(self):
        return self.idCred
    
    # Cela semble obligatoire pour la generation de la base
    class Meta:
        app_label='LEOGoals'
