# -*- coding: utf-8 -*-

from django.utils.translation import ugettext_lazy as _
from django.contrib import admin
from django import forms
from LEOGoals.models.UserDesc import UserDesc
from LEOGoals.models.Credential import Credential
from LEOGoals.models.Vignette import Vignette
from taggit.models import Tag
from LEOGoals.models.OrderedVignetteSet import OrderedVignetteSet, VignetteRank




class UserDescAdmin(admin.ModelAdmin):
    fieldsets = (
        ('', {
            'fields': ('name', 'forename', 'birthdate'),
        }),
        ('Flags', {
            'classes': ('grp-collapse grp-closed',),
            'fields' : ('comments',),
        }),
                 )

class CredentialAdmin(admin.ModelAdmin):
    pass

class VignetteAdmin(admin.ModelAdmin):
    def thumb(self,instance):
        if not instance.image:
            return ""
        else:
            return '<img src="' + instance.image + '" alt="Vignette" height="60" width="60">'
    thumb.short_description = _(u'Thumbnail')
    thumb.allow_tags = True
    
    list_display = ( 'thumb', 'title', 'description')
    search_fields = ['title', 'description']
    #list_editable = [ 'title',]
    list_display_links = ('title', 'thumb')
    fieldsets = (
        ('', {
            'fields': ('title', 'image', 'tags', 'description'),
        }),
    )



class VignetteRankInline(admin.TabularInline):
    model = VignetteRank
    extra = 1


class OrderedVignetteSetAdmin(admin.ModelAdmin):
    list_display = ( 'title', )
    search_fields = ['title',]
    #list_editable = [ 'title',]
    list_display_links = ('title', )
    inlines =(VignetteRankInline,)
    fieldsets = (
        ('', {
            'fields': ('title', ),
        }),
#        ('Vignettes', {
#            'classes': ('grp-collapse grp-open',),
#            'fields' : (),
#        }),
    )
    

class MyTagAdmin(admin.ModelAdmin):
    list_display = ["name","slug"]
    search_fields = ('name','slug')
    
# Override Taggit's admin
admin.site.unregister(Tag)
admin.site.register(Tag,MyTagAdmin)


admin.site.register(UserDesc,UserDescAdmin)
admin.site.register(Credential,CredentialAdmin)
admin.site.register(Vignette,VignetteAdmin)
admin.site.register(OrderedVignetteSet,OrderedVignetteSetAdmin)
