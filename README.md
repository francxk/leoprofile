Les fonctions réalisées
=======================

Par défaut, l'interface utilisateur de gestion des données
----------------------------------------------------------
Interface par défaut, donc pas top mais elle est fontionnelle.

On la trouve ici :<http://localhost:8000/admin>
* gestion des objectifs pour la consomation de flux media (LEOGoals)
* gestion des programmes TV


Widgets pour le tri des vignettes
----------------------------------
Ce tri de vignette permette d'arbitrer les préférences éducatives ou de médiation entre enfants et parents. 
La configuration des vignettes se fait sur l'interface d'administration
<http://localhost:8000/admin/LEOGoals/vignette/>

Tri d'un '''jeu de vignette''', autant de jeu que de profil : <http://localhost:8000/leogoals/vignettesort/noprofile/>

Par exemple on a un profil matin, midi et soir et pour chacun on a un jeu de carte déterminant des orientations différentes.

Intégration des programmes TV
-----------------------------
On a un point d'accès <http://localhost:8000/tvprogram/rest/pushprogram/> qui consomme des lux xmltv
On peut alimenter la base de données grace à un script qui donne les programmes TV

    ./tv_grab_fr_iphone.pl --days 7 | curl --data-binary @- http://localhost:8000/tvprogram/rest/pushprogram/

Evaluation d'un score d'un profile sur un texte
-----------------------------------------------
C'est un Web Service, le numéro de profile est en fin d'URL, on envoie sur cette URL le
texte à analyser.

Ex <http://machine:8000/tvprogram/rest/scoring/1>

Le retour ressemble à
    
    [
        [
            "vignette name1", # nom de la vignette en position 1
            {
                "word1": 4, # nombre de fois que le mot word1 est trouvé dans le texte
                "word2": 2,
            }
        ], 
        [
            "vignette name2", 
            {}
        ]
    ]           


Modèle de données
==================
![Texte alternatif](../LEOGoals/static/doc/model.png "Modèle de données de LEOGoals")

Quelques commandes
==================
Développement
-------------

* Génération **graphique UML** du modèle de donnees

		python manage.py graph_models LEOGoals tv_program taggit -e -g -o LEOGoals/static/doc/model.png

* Manipulation de la base de données

    * Sauvegarde BDs par script (pb avec Many2Many)

			python manage.py dumpscript LEOGoals > LEOGoalscripts/XX.py

    * Rechargement

			python manage.py runscript scripts/XX.py

    * Export données

			python manage.py dumpdata LEOGoals tv_program taggit --indent 1 > LEOGoals/fixtures/bd.version.json

    * Import données

			python manage.py loaddata LEOGoals/fixtures/bd.version.json
